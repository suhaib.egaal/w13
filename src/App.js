import React from 'react';
import './App.css';

function App() {
  
  const weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  return (
    <div className="App">
      <header className="App-header">
        <h2>Weekdays in table</h2>
        <table>
          <thead>
            <tr>
              <th>Weekday</th>
            </tr>
          </thead>
          <tbody>
            {weekdays.map((weekday, index) => (
              <tr key={index}>
                <td>{weekday}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </header>
    </div>
  );
}

export default App;
